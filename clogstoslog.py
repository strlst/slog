#!/usr/bin/env python3
import json

def create_log(key, sorting, comment, sublists, items):
	return {"key": key, "sort": sorting, "comment": comment, "lists": sublists, "entries": items,}

def walklogs(logs, root):
	key = logs['key']
	leaf = logs['leaf']
	sorting = logs['sorting']
	comment = logs['comment']
	items = logs['items']
	#print(f'Log(key={key}, leaf={leaf}, sorting={sorting}, comment={comment}, itemcount={len(items)})')
	new_log = create_log(key, sorting, comment, [], items if leaf else [])
	if not leaf:
		root['lists'].append(new_log)
		for item in items:
			walklogs(item, new_log)
	else:
		root['lists'].append(new_log)

def main():
	filepath = '/home/strlst/.local/share/strlst'
	root = create_log('root', True, '', [], [])
	with open(f'{filepath}/default.clogs', 'r') as clogs_file:
		logs = json.load(clogs_file)
		print(f'read clogs file at "{filepath}/default.clogs"')
		for item in logs['items']:
			walklogs(item, root)
	print(f'converted clogs to slog')
	with open(f'{filepath}/default.slog', 'w') as ulogs_file:
		json.dump(root, ulogs_file)
		print(f'dumped ulogs at "{filepath}/default.slog"')

if __name__ == "__main__":
	main()
