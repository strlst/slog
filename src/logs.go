package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type Log struct {
	Key     string   `json:"key"`
	Sort    bool     `json:"sort"`
	Comment string   `json:"comment"`
	Lists   []Log    `json:"lists"`
	Entries []string `json:"entries"`
}

func readLogsFile(program string) []byte {
	// query environment
	user, userOk := os.LookupEnv("USER")
	home, homeOk := os.LookupEnv("HOME")
	if !(userOk && homeOk) {
		fmt.Fprintf(os.Stderr, "%s: error: environment variables $USER and $HOME need to be set\n", program)
		os.Exit(EXIT_ENV_INVALID)
	}

	defaultLogsFilepath := fmt.Sprintf("%s/.local/share/%s/default.slog", home, user)

	content, err := os.ReadFile(defaultLogsFilepath)
	if os.IsNotExist(err) {
		fmt.Fprintf(os.Stderr, "%s: specified logs file \"%s\" does not exist\n", program, defaultLogsFilepath)
		os.Exit(EXIT_SPECIFIED_PATH_INVALID)
	}

	return content
}

func readLogs(program string, data []byte) Log {
	var logs Log
	err := json.Unmarshal(data, &logs)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: could not unmarshal json file\n", program)
		os.Exit(EXIT_JSON_ERROR)
	}
	return logs
}
