package main

import "strings"

const (
	padLeftString         string = "   "
	padLeftStringSelected string = " | "
	padRightString        string = ";"
	padLeftLength         int    = len(padLeftString)
	padRightLength        int    = len(padRightString)
)

func CreateCell(active *Active, position int, colorCode string) string {
	// collect all line information in a string builder
	var lineBuilder strings.Builder

	// determine cell
	var cell string
	if position < len(active.log.Lists) {
		cell = active.log.Lists[position].Key
	} else if position < len(active.log.Lists)+len(active.log.Entries) {
		cell = active.log.Entries[position]
	}

	// pad left and write actual cell name
	if position == active.selected {
		lineBuilder.WriteString(colorCode)
		lineBuilder.WriteString(padLeftStringSelected)
		lineBuilder.WriteString(cell)
		lineBuilder.WriteString(STYLE_RESET)
	} else {
		lineBuilder.WriteString(padLeftString)
		lineBuilder.WriteString(cell)
	}

	// pad right
	lineBuilder.WriteString(strings.Repeat(" ", active.lineWidth-len(cell)-padLeftLength))
	// extract string
	return lineBuilder.String()
}
