package main

import (
	"fmt"
	"strings"

	tea "github.com/charmbracelet/bubbletea"
)

const (
	STYLE_RESET        = "\033[0m"
	STYLE_BOLD         = "\033[1m"
	STYLE_UNDERLINE    = "\033[4m"
	STYLE_STRIKE       = "\033[9m"
	STYLE_ITALIC       = "\033[3m"
	COLOR_RED          = "\033[0;31m"
	COLOR_GREEN        = "\033[0;32m"
	COLOR_YELLOW       = "\033[0;33m"
	COLOR_BLUE         = "\033[0;34m"
	COLOR_PURPLE       = "\033[0;35m"
	COLOR_CYAN         = "\033[0;36m"
	COLOR_WHITE        = "\033[0;37m"
	COLOR_LIGHT_RED    = "\033[1;31m"
	COLOR_LIGHT_GREEN  = "\033[1;32m"
	COLOR_LIGHT_YELLOW = "\033[1;33m"
	COLOR_LIGHT_BLUE   = "\033[1;34m"
	COLOR_LIGHT_PURPLE = "\033[1;35m"
	COLOR_LIGHT_CYAN   = "\033[1;36m"
	COLOR_LIGHT_WHITE  = "\033[1;37m"
)

func (m *Model) NextList() {
	active := m.active[m.focused]
	if active.selected < active.listCount {
		list := &active.log.Lists[active.selected]
		next := CreateActive(list)
		m.active = append(m.active, next)
		m.focused++
	}
}

func (m *Model) PrevList() {
	if m.focused > 0 {
		m.active = m.active[:len(m.active)-1]
		m.focused--
	}
}

func (m Model) Init() tea.Cmd {
	return nil
}

func (a *Active) Update(height int) {
	// reset values
	a.lines = a.lines[:0]

	start, end := a.ScrollView(height)
	for row := start; row < end; row++ {
		a.lines = append(a.lines, CreateCell(a, row, COLOR_LIGHT_GREEN))
	}
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	// small helper
	active := &m.active[m.focused]

	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.windowWidth, m.windowHeight = msg.Width, msg.Height
	case tea.KeyMsg:
		switch keypress := msg.String(); keypress {
		case "q", "ctrl+c":
			return m, tea.Quit
		case "j", "down":
			combined := len(m.active[m.focused].log.Lists) + len(m.active[m.focused].log.Entries)
			active.selected = min(active.selected+1, combined-1)
		case "k", "up":
			active.selected = max(active.selected-1, 0)
		case "h", "left":
			m.PrevList()
		case "l", "right":
			m.NextList()
		case "g", "PgUp":
			active.selected = 0
		case "G", "PgDn":
			active.selected = active.totalCount - 1
		case "f1":
			m.showDebug = !m.showDebug
		case "t":
			//v := !m.lists[m.focused].ShowTitle()
			//m.lists[m.focused].SetShowTitle(v)
		}
	}

	// as an optimization, update only current active
	active.Update(m.windowHeight)

	var cmd tea.Cmd
	return m, cmd
}

func (m Model) View() string {
	maxHeight := 0
	for _, a := range m.active {
		height := len(a.log.Lists) + len(a.log.Entries)
		if height > maxHeight {
			maxHeight = min(m.windowHeight, height)
		}
	}

	// begin building the string
	var view strings.Builder
	for row := 0; row < maxHeight; row++ {
		for _, a := range m.active {
			if len(a.lines) > row {
				view.WriteString(a.lines[row])
			} else {
				view.WriteString(strings.Repeat(" ", a.lineWidth))
			}
		}
		view.WriteRune('\n')
	}

	// debug option
	if m.showDebug {
		// debug string view
		var debug strings.Builder
		debug.WriteString("--- DEBUG ---\n")
		debug.WriteString(fmt.Sprintf("logs=%p focused=%v actives=%v\n", m.log, m.focused, len(m.active)))
		for _, a := range m.active {
			debug.WriteString(fmt.Sprintf("logs=%p selected=%v listCount=%v totalCount=%v\n", a.log, a.selected, a.listCount, a.totalCount))
		}
		debug.WriteString("-------------\n")
		debug.WriteRune('\n')
		// finally return view
		return debug.String() + view.String()
	} else {
		return view.String()
	}
}
