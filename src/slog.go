package main

import (
	"log"
	"os"
	"strings"

	tea "github.com/charmbracelet/bubbletea"
)

const (
	EXIT_SUCCESS int = iota
	EXIT_ENV_INVALID
	EXIT_SPECIFIED_PATH_INVALID
	EXIT_JSON_ERROR
)

func main() {
	// query program name by call
	program := os.Args[0][strings.LastIndex(os.Args[0], "/")+1 : len(os.Args[0])]

	// read logs file
	logsFile := readLogsFile(program)

	// parse logs file
	logs := readLogs(program, logsFile)

	// create model out of logs
	m := NewModel(&logs)
	p := tea.NewProgram(m, tea.WithAltScreen())
	if _, err := p.Run(); err != nil {
		log.Fatal(err)
	}
}
