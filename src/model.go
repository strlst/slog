package main

// implement list item interface
func (l Log) FilterValue() string {
	return l.Key
}

func (l Log) Title() string {
	return l.Key
}

func (l Log) Description() string {
	return l.Comment
}

type Active struct {
	log        *Log
	selected   int
	lines      []string
	lineWidth  int
	listCount  int
	totalCount int
}

type Model struct {
	log          *Log
	active       []Active
	focused      int
	showDebug    bool
	windowWidth  int
	windowHeight int
}

func CreateActive(list *Log) Active {
	width := 0
	listCount := len(list.Lists)
	totalCount := len(list.Lists) + len(list.Entries)
	for _, l := range list.Lists {
		if len(l.Key) > width {
			width = len(l.Key)
		}
	}
	for _, e := range list.Entries {
		if len(e) > width {
			width = len(e)
		}
	}

	active := Active{
		log:        list,
		lineWidth:  width + padLeftLength + padRightLength,
		selected:   0,
		listCount:  listCount,
		totalCount: totalCount,
	}
	active.Update(0)
	return active
}

func (a Active) ScrollView(height int) (start int, end int) {
	if a.totalCount-1 > height {
		return 0, a.totalCount
	}

	// TODO: implement scrolling as before
	return 0, a.totalCount
}

func NewModel(log *Log) *Model {
	return &Model{
		log: log,
		active: []Active{
			CreateActive(log),
		},
	}
}
