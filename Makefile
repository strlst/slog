CXX:=go
GOBUILD:=$(CXX) build
GOCLEAN:=$(CXX) clean

PROG:= slog
SOURCES:= src/slog.go src/tea.go src/logs.go src/model.go src/util.go

all: build run

build:
	$(GOBUILD) $(SOURCES)

release:
	$(GOBUILD) -ldflags="-s -w" $(SOURCES)

install: release
	cp $(PROG) /usr/local/bin

uninstall:
	rm -f /usr/local/bin/$(PROG)

run:
	./$(PROG)

clean:
	$(GOCLEAN)

test:
	./$(PROG) list
